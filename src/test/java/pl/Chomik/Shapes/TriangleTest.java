package pl.Chomik.Shapes;

import org.junit.Test;

import static org.junit.Assert.*;

public class TriangleTest {


    private Shapes triangle = new Triangle(4,5, 6,7);

    @Test
    public void getArea() {

        double area = triangle.getArea();

        assertEquals(area,10,0.0);
    }

    @Test
    public void getCircuit() {

        double circuit = triangle.getCircuit();

        assertEquals(circuit,17, 0.0 );
    }
}