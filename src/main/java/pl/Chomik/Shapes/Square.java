package pl.Chomik.Shapes;

public class Square implements Shapes {
    double dimension;

    public Square (double dimension){
        this.dimension = dimension;
    }

    @Override
    public double getArea() {
        return dimension*dimension;
    }

    @Override
    public double getCircuit() {
        return 4*dimension;
    }
}
